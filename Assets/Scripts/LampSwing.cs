using System.Linq;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;

public class LampSwing : MonoBehaviour
{
    public LineRenderer LineRenderer;
    public Rigidbody2D Rigidbody2D;
    public Transform LineTarget;

    private void Awake()
    {
        LineRenderer = GetComponentInParent<LineRenderer>();
        Rigidbody2D = GetComponent<Rigidbody2D>();
        LineTarget = gameObject.Children().First(obj => obj.name == "LineTarget").transform;

        var force = Vector2.right * .025f * FloatRange.ZeroOne.Random();
        Rigidbody2D.AddForceAtPosition(force, Vector2.down * .01f, ForceMode2D.Impulse);

        this.UpdateAsObservable()
            .SampleFrame(0)
            .SubscribeWithState(this, (_, self) =>
            {
                self.UpdateLine();
            })
            .AddTo(this);
    }

//    private void Update()
//    {
//        if (Input.GetKeyDown(KeyCode.F))
//        {
//            Rigidbody2D.AddForceAtPosition(Vector2.right * .025f, Vector2.down * .01f, ForceMode2D.Impulse);
//        }
//    }

    private void Shake()
    {
        Rigidbody2D.AddForce(Vector2.down * .5f, ForceMode2D.Impulse);
    }

    private void UpdateLine()
    {
        LineRenderer.SetPosition(1, LineTarget.position - LineRenderer.transform.position);
    }
}
