﻿using UniRx;

public class EventStore : MonoSingletonExt<EventStore>
{
    public Subject<float> DirectorSendCrate;
    public Subject<Unit> DirectorCheckOn;
    public Subject<float> DirectorYell;
    public Subject<float> DirectorYellWakeUp;
    public Subject<float> DirectorYellWork;
    public Subject<Unit> GunIsPickedUp;
    public Subject<Unit> GameOver;
    public Subject<float> WakeUp;

    private void OnEnable()
    {
        DirectorSendCrate = new Subject<float>().AddTo(this);
        DirectorCheckOn = new Subject<Unit>().AddTo(this);
        DirectorYell = new Subject<float>().AddTo(this);
        DirectorYellWakeUp = new Subject<float>().AddTo(this);
        DirectorYellWork = new Subject<float>().AddTo(this);
        GunIsPickedUp = new Subject<Unit>().AddTo(this);
        GameOver = new Subject<Unit>().AddTo(this);
        WakeUp = new Subject<float>().AddTo(this);
    }

//    private void OnDisable()
//    {
//        GameStateObservable.Dispose();
//        DirectorSendCrate.Dispose();
//        DirectorCheckOn.Dispose();
//        DirectorYell.Dispose();
//        DirectorYellWork.Dispose();
//        SlackOff.Dispose();
//        WakeUp.Dispose();
//        FallAsleep.Dispose();
//    }
}

