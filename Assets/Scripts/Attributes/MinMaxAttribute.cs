using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class MinMaxAttribute : PropertyAttribute
{
    public readonly float Min;
    public readonly float Max;
    public readonly float Step;
    public readonly int DecimalPlaces;

    public MinMaxAttribute(float min, float max, int decimalPlaces, float step = float.Epsilon)
    {
        Min = min;
        Max = max;
        Step = step;
        DecimalPlaces = decimalPlaces;
    }
}
