using System;
using Extensions;
using UniRx;
using UnityEngine;

public class ZoneScore : MonoBehaviour
{
    public BoxSpawner Spawner;

    public IntReactiveProperty CrateCount;
    public float Score;

    [SerializeField]
    private float _sizeRatio;

    private void Start()
    {
        CrateCount
            .SubscribeWithState(this, (count, self) => self.Score = count * self._sizeRatio)
            .AddTo(this);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.CRATE))
        {
            CrateCount.Value++;

            if (Math.Abs(_sizeRatio) < float.Epsilon)
            {
                var crateArea = Vector2.Scale(((BoxCollider2D) other).size, other.transform.lossyScale.XY()).Area();
                var zoneArea = GetComponent<BoxCollider2D>().size.Area();
                _sizeRatio = crateArea / zoneArea;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.CRATE))
        {
            CrateCount.Value--;
        }
    }
}
