using System.Collections;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Video;

public class PrepareCrate : MonoBehaviour
{
    private void OnEnable()
    {
        this.OnCollisionEnter2DAsObservable()
            .SkipWhile(collision => !collision.gameObject.CompareTag(StringLookup.Tags.FLOOR))
            .Take(1)
            .SubscribeWithState(this,
                (_, self) => self.UpdateLayer())
            .AddTo(this);

        StartCoroutine(UpdateLayerCor(3));
    }

    private void UpdateLayer()
    {
        gameObject.layer = LayerMask.NameToLayer(StringLookup.Layers.DEFAULT);
    }

    private IEnumerator UpdateLayerCor(float delay)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
            yield return null;

        UpdateLayer();
    }
}
