using System.Collections;
using UnityEngine;

public class ColorLooper : MonoBehaviour
{
    public Color[] Colors;
    public float DurationEach;

    private SpriteRenderer _renderer;

    private void OnEnable()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.color = Colors[0];

        StartCoroutine(ColorLoopCoroutine());
    }

    private IEnumerator ColorLoopCoroutine()
    {
        var length = Colors.Length;
        int nextIndex;
        for (var i = 0; isActiveAndEnabled; i = nextIndex)
        {
            nextIndex = (i + 1) % length;

            var colorDelta = (Colors[nextIndex] - Colors[i]) / DurationEach;

            var endTime = Time.time + DurationEach;
            while (Time.time < endTime)
            {
                _renderer.color += colorDelta * Time.deltaTime;
                yield return null;
            }
        }
    }
}
