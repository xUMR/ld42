using UniRx;
using UnityEngine;

public class DoorLight : MonoBehaviour
{
    public Light Light;
    public float MaxIntensity;
    public float HalfDuration;

    [SerializeField]
    private bool _gunIsPickedUp;

    private void OnEnable()
    {
        EventStore.instance.GunIsPickedUp
            .SubscribeWithState(this, (_, self) =>
            {
                self.gameObject.SetActive(true);
                self._gunIsPickedUp = true;
                LeanTween.delayedCall(3, () => self.gameObject.SetActive(true));
            })
            .AddTo(this);

        Light = Light == null ? GetComponent<Light>() : Light;

        LeanTween.value(gameObject, f => Light.intensity = f, 0, MaxIntensity, HalfDuration)
            .setEaseOutExpo();

        if (_gunIsPickedUp)
            return;

        LeanTween.value(gameObject, f => Light.intensity = f, MaxIntensity, 0, HalfDuration)
            .setDelay(HalfDuration)
            .setEaseInExpo();

        LeanTween.delayedCall(HalfDuration * 2, () => gameObject.SetActive(false));

//        LeanTween.value(0, MaxIntensity, HalfDuration).setEaseOutCubic();
//        LeanTween.value(MaxIntensity, 0, HalfDuration).setDelay(HalfDuration).setEaseOutCubic();
    }
}
