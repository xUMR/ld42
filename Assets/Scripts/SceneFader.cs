using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class SceneFader : MonoBehaviour
{
    public RectTransform Panel;
    public bool FadeFromBlackOnAwake;

    private void Awake()
    {
        if (FadeFromBlackOnAwake)
            FadeFromBlack(1, 0);
    }

    public void FadeToBlack(float duration, float delay)
    {
        LeanTween.alpha(Panel, 1, duration).setDelay(delay).setEaseInOutSine();
    }

    public void FadeFromBlack(float duration, float delay)
    {
//        Panel.GetComponent<Image>().color = Color.black;
        LeanTween.alpha(Panel, 0, duration).setDelay(delay).setEaseInOutSine();
    }
}
