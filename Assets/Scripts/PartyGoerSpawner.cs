using UnityEngine;

public class PartyGoerSpawner : MonoBehaviour
{
    public GameObject Prefab;

    public int Count;

    private GameObject[] _elements;

    private void Start()
    {
        var rangeX = FloatRange.MinusOneOne * 4 - transform.GetPositionX() / 2;
        var count = FloatRange.ZeroOne * Count;

        for (var i = 0; i < Count; i++)
        {
//            Instantiate(Prefab, transform);

            var x = count.MapTo(rangeX, i);

            var go = Instantiate(Prefab, new Vector3(x, 1), Quaternion.identity);
            go.transform.SetParent(transform);
        }
    }

//    private void Update()
//    {
//
//    }
}
