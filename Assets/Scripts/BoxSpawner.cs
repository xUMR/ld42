using System.Collections;
using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    public GameObject Prefab;
    public FloatRange ForceRange;
    public FloatRange TorqueRange;

    public int SpawnedCount;

    [SerializeField]
    private int _layer;

    private void Awake()
    {
        _layer = LayerMask.NameToLayer(StringLookup.Layers.NEW_CRATE);
    }

    public void SpawnWithForce(float delay = 0)
    {
        StartCoroutine(SpawnCoroutine(delay, true));
    }

    private IEnumerator SpawnCoroutine(float delay = 0, bool applyForce = false)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
            yield return null;

        var obj = Instantiate(Prefab, transform.position, Quaternion.identity);
        // some fall through the ramp
        if (Random.value > .5f)
            obj.layer = _layer;

        if (applyForce)
            ApplyForce(obj.GetComponent<Rigidbody2D>());

        SpawnedCount++;
    }

    public void ApplyForce(Rigidbody2D rigidbody2D)
    {
        var force = Vector2.right * ForceRange.Random();
        rigidbody2D.AddForce(force, ForceMode2D.Impulse);

        var torque = TorqueRange.Random();
        rigidbody2D.AddTorque(torque, ForceMode2D.Impulse);
    }
}
