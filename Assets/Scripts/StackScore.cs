using System;
using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class StackScore : MonoBehaviour
{
    public int Accuracy;
    private readonly RaycastHit2D[] _results = new RaycastHit2D[2];

    private void OnEnable()
    {
        this.UpdateAsObservable()
            .Sample(TimeSpan.FromSeconds(.25))
            .SubscribeWithState(this, (_, self) =>
            {
                self.CheckPosition();
            })
            .AddTo(this);
    }

    private void CheckPosition()
    {
        var start = transform.position;
        var end = start.AddY(-.5f);

        var hits = Physics2D.LinecastNonAlloc(start, end, _results);
        Accuracy = 0;
        if (hits <= 1)
            return;

        var other = _results[1].collider;

        if (!other.CompareTag(StringLookup.Tags.CRATE))
            return;

//        Accuracy = 100 - Mathf.RoundToInt(Mathf.Abs(other.transform.GetPositionX() - transform.GetPositionX()) / transform.localScale.x * 100);
        Accuracy = AccuracyPercentage(other.transform.GetPositionX(), transform.GetPositionX());
    }

    public static int AccuracyPercentage(float measured, float accepted)
    {
        return 100 - Mathf.RoundToInt(Mathf.Abs(measured - accepted) / accepted * 100);
    }
}
