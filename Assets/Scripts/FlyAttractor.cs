using System.Collections;
using Extensions;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;

public class FlyAttractor : MonoBehaviour
{
    public GameObject FlyPrefab;
    public FloatRange DurationModifier;

    private GameObject[] _flies;

    private void OnEnable()
    {
        _flies = new GameObject[8];
        for (var i = 0; i < _flies.Length; i++)
        {
            var fly = Instantiate(FlyPrefab, transform);
            fly.transform.position = transform.position;
            _flies[i] = fly;
            Fly(fly.transform, .2f, 0);
        }

//        this.UpdateAsObservable()
//            .Do(_ => MakeFliesFly())
//            .Sample(System.TimeSpan.FromSeconds(60))
//            .SubscribeWithState(this, (_, self) => self.MakeFliesFly());
    }

    private void OnDisable()
    {
        foreach (var fly in _flies)
        {
            fly.gameObject.Destroy();
        }
    }

//    private void MakeFliesFly()
//    {
//        print("Making flies fly!");
//        foreach (var fly in _flies)
//        {
//            var target = transform.position.XY() + new Vector2(Random.value - .5f, Random.value - .5f) * 2;
//            LeanTween.move(fly, target, .3f).setDelay(Random.value / 8).setLoopPingPong().setEaseInOutBounce();
//        }
//    }

    private void Fly(Transform fly, float duration, float delay)
    {
        var flyCoroutine = FlyCoroutine(fly, duration, delay);
//        MainThreadDispatcher.StartUpdateMicroCoroutine(flyCoroutine);
        StartCoroutine(flyCoroutine);
    }

    private IEnumerator FlyCoroutine(Transform fly, float duration, float delay)
    {
        while (isActiveAndEnabled)
        {
            var endTime = Time.time + delay;
            while (Time.time < endTime)
                yield return null;

            var target = transform.position.XY() + new Vector2(Random.value - .5f, Random.value - .5f);
            var v = target - fly.position.XY();

            endTime = Time.time + duration + DurationModifier.Random();
            while (Time.time < endTime)
            {
                fly.Translate(v * Time.deltaTime);
                yield return null;
            }
        }
    }
}
