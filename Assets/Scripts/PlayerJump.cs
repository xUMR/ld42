using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    public Inputs Inputs;

//    [HideInInspector]
    public Rigidbody2D Rigidbody2D;

    public PlayerPickUp PlayerPickUp;

    public float Magnitude;
    public float MagnitudeWithCrate;

    private readonly RaycastHit2D[] _results = new RaycastHit2D[2];

    public LayerMask GroundLayer;

    public bool IsJumping;

    private void Start()
    {
        Rigidbody2D = GetComponentInParent<Rigidbody2D>();
        PlayerPickUp = transform.parent.GetComponentInChildren<PlayerPickUp>();

        Inputs.Jump
            .Where(_ => !IsJumping)
            .SubscribeWithState(this, (_, self) =>
            {
                var magnitude = self.PlayerPickUp.IsHoldingObject ? self.MagnitudeWithCrate : self.Magnitude;
                var movement = magnitude * self.transform.up;
                self.Rigidbody2D.velocity = self.Rigidbody2D.velocity.WithY(movement.y);
            })
            .AddTo(this);

//        Inputs.Jump
//            .SubscribeWithState(this, (_, self) =>
//            {
//                var time = 0.4f;
//                var positionY = self.transform.position.y;
//                var rootObject = self.GetRootTransform().gameObject;
//                LeanTween.moveY(rootObject, positionY + self.Magnitude, time).setEaseOutCubic();
//                LeanTween.moveY(rootObject, positionY, time).setEaseInCubic().setDelay(time);
//            });

        this.FixedUpdateAsObservable()
            .SubscribeWithState(this, (_, self) =>
            {
                var start = self.transform.position;
                var end = start.AddY(-.6f);
                Debug.DrawLine(start, end, Color.magenta);
                var hitCount = Physics2D.LinecastNonAlloc(start, end, self._results, self.GroundLayer);
                self.IsJumping = hitCount < 1;

                if (!self.IsJumping) return;

                start = start + new Vector3(-.2f, -.5f);
                end = start.AddX(.4f);
                Debug.DrawLine(start, end, Color.magenta);
                hitCount = Physics2D.LinecastNonAlloc(start, end, self._results, self.GroundLayer);
                self.IsJumping = hitCount < 1;
            });
    }
}
