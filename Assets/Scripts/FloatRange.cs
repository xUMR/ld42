﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Extensions;
using UnityEngine;

[Serializable]
public struct FloatRange
{
    private static readonly FloatRange ZeroRange = new FloatRange(0, 0);
    private static readonly FloatRange ZeroOneRange = new FloatRange(0, 1);
    private static readonly FloatRange MinusOneZeroRange = new FloatRange(-1, 0);
    private static readonly FloatRange MinusOneOneRange = new FloatRange(-1, 1);
    private static readonly FloatRange PositiveRange = new FloatRange(0 + float.Epsilon, float.PositiveInfinity);
    private static readonly FloatRange NegativeRange = new FloatRange(float.NegativeInfinity, 0 - float.Epsilon);

    public float Min;
    public float Max;

    public FloatRange(float min, float max)
    {
        if (min > max)
            throw new ArgumentException("Min can't be larger than max");

        Min = min;
        Max = max;
    }

    public FloatRange(float max) : this(0, max) { }

    public static FloatRange Of(float min, float max)
    {
        return new FloatRange(min, max);
    }

    public static FloatRange Of(float max)
    {
        return new FloatRange(0, max);
    }

    public float Avg => Min + Max / 2;

    public float Range => Max - Min;
    public float HalfRange => (Max - Min) / 2;


    #region contains methods

    public bool Contains(int n)
    {
        return n >= Min && n < Max;
    }

    public bool ContainsInclusive(int n)
    {
        return n >= Min && n <= Max;
    }

    public bool Contains(float n)
    {
        return n >= Min && n < Max;
    }

    public bool ContainsInclusive(float n)
    {
        return n >= Min && n <= Max;
    }

    public bool Contains(FloatRange range)
    {
        return Min <= range.Min && Max >= range.Max;
    }

    #endregion

    #region random methods

    public float Random()
    {
        return UnityEngine.Random.value * (Max - Min) + Min;
    }

    public int RandomInt()
    {
        return (int) (UnityEngine.Random.value * (Max - Min) + Min);
    }

    public Vector2 RandomVector2()
    {
        return new Vector2(Random(), Random());
    }

    public Vector3 RandomVector3()
    {
        return new Vector3(Random(), Random(), Random());
    }

    #endregion

    [SuppressMessage("ReSharper", "ConvertIfStatementToReturnStatement")]
    public float Clamp(float f)
    {
        if (f < Min) return Min;
        if (f > Max) return Max;
        return f;
    }

    public static FloatRange Lerp(FloatRange range0, FloatRange range1, float t)
    {
        return new FloatRange(Mathf.Lerp(range0.Min, range1.Min, t), Mathf.Lerp(range0.Max, range1.Max, t));
    }

    public float Lerp(float value)
    {
        return Mathf.Lerp(Min, Max, value);
    }

    public static float Map(float value, FloatRange from, FloatRange to)
    {
        var f = (value - from.Min) / (from.Max - from.Min);
        return Mathf.Lerp(to.Min, to.Max, f);
    }

    public float MapFrom(FloatRange other, float value)
    {
        var f = (value - other.Min) / (other.Max - other.Min);
        return Mathf.Lerp(Min, Max, f);
    }

    public float MapTo(FloatRange other, float value)
    {
        var f = (value - Min) / (Max - Min);
        return Mathf.Lerp(other.Min, other.Max, f);
    }

    public override string ToString()
    {
        return string.Format("({0}, {1})", Min, Max);
    }

    public float this[int index]
    {
        get
        {
            switch (index)
            {
                case 0: return Min;
                case 1: return Max;
                default: throw new IndexOutOfRangeException("Invalid FloatRange index!");
            }
        }
        set
        {
            switch (index)
            {
                case 0:
                    Min = value;
                    break;
                case 1:
                    Max = value;
                    break;
                default:
                    throw new IndexOutOfRangeException("Invalid FloatRange index!");
            }
        }
    }

    public Enumerable AsEnumerable(float step = 1, bool isInclusive = false)
    {
        return new Enumerable(this, step, isInclusive);
    }

    #region with & add & multiply methods

    public FloatRange WithMin(float min)
    {
        return new FloatRange(min, Max);
    }

    public FloatRange WithMax(float max)
    {
        return new FloatRange(Min, max);
    }

    public FloatRange AddMin(float offset)
    {
        return new FloatRange(Min + offset, Max);
    }

    public FloatRange AddMax(float offset)
    {
        return new FloatRange(Min, Max + offset);
    }

    public FloatRange MultiplyMin(float multiplier)
    {
        return new FloatRange(Min * multiplier, Max);
    }

    public FloatRange MultiplyMax(float multiplier)
    {
        return new FloatRange(Min, Max * multiplier);
    }

    #endregion

    #region static properties

    public static FloatRange Zero
    {
        get { return ZeroRange; }
    }

    public static FloatRange ZeroOne
    {
        get { return ZeroOneRange; }
    }

    public static FloatRange MinusOneZero
    {
        get { return MinusOneZeroRange; }
    }

    public static FloatRange MinusOneOne
    {
        get { return MinusOneOneRange; }
    }

    public static FloatRange Positive
    {
        get { return PositiveRange; }
    }

    public static FloatRange Negative
    {
        get { return NegativeRange; }
    }

    #endregion

    #region operators

    public static FloatRange operator +(FloatRange r0, FloatRange r1)
    {
        if (r0.Contains(r1))
            return r0;
        if (r1.Contains(r0))
            return r1;

        if (Mathf.Approximately(r0.Max, r1.Min))
            return new FloatRange(r0.Min, r1.Max);
        if (Mathf.Approximately(r1.Max, r0.Min))
            return new FloatRange(r1.Min, r0.Max);

        throw new ArgumentException(string.Format("Cannot combine ranges {0} and {1} into a single range", r0, r1));
    }

    public static FloatRange operator +(FloatRange range, float f)
    {
        return new FloatRange(range.Min + f, range.Max + f);
    }

    public static FloatRange operator +(float f, FloatRange range)
    {
        return new FloatRange(range.Min + f, range.Max + f);
    }

    public static FloatRange operator -(FloatRange r0, FloatRange r1)
    {
        // (0, 3) - (0, 3) = (0, 0)

        // (0, 3) - (1, 3) = (0, 1)
        // (0, 3) - (0, 2) = (2, 3)

        if (Mathf.Approximately(r0.Min, r1.Min) && Mathf.Approximately(r0.Max, r1.Max))
            return ZeroRange;
        if (Mathf.Approximately(r0.Min, r1.Min) && r0.Max >= r1.Max)
            return new FloatRange(r1.Max, r0.Max);
        if (Mathf.Approximately(r0.Max, r1.Max) && r0.Max >= r1.Max)
            return new FloatRange(r0.Min, r1.Min);

        throw new ArgumentException(string.Format("Invalid operation: {0} - {1} ", r0, r1));
    }

    public static FloatRange operator -(FloatRange range, float f)
    {
        return new FloatRange(range.Min - f, range.Max - f);
    }

    public static FloatRange operator -(float f, FloatRange range)
    {
        return new FloatRange(range.Min - f, range.Max - f);
    }

    public static FloatRange operator *(FloatRange range, float f)
    {
        return new FloatRange(range.Min * f, range.Max * f);
    }

    public static FloatRange operator *(float f, FloatRange range)
    {
        return new FloatRange(range.Min * f, range.Max * f);
    }

    #endregion

    public struct Enumerable : IEnumerable<float>
    {
        private readonly float _min;
        private readonly float _max;
        private readonly float _step;
        private readonly bool _isInclusive;

        internal Enumerable(float min, float max, float step, bool isInclusive)
        {
            if (Mathf.Approximately(step, 0))
                throw new ArgumentException("Step cannot be 0.");

            _min = min;
            _max = max;
            _step = step;
            _isInclusive = isInclusive;
        }

        internal Enumerable(FloatRange range, float step = 1, bool isInclusive = false) : this(range.Min, range.Max,
            step, isInclusive) { }

        public IEnumerator<float> GetEnumerator()
        {
            if (_step > 0)
            {
                var value = _min;
                while (value < _max)
                {
                    yield return value;
                    value += _step;
                }
                if (_isInclusive)
                    yield return _max;
            }
            else
            {
                var value = _max;
                while (value > _min)
                {
                    yield return value;
                    value += _step;
                }
                if (_isInclusive)
                    yield return _min;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public float this[int index]
        {
            get
            {
                if (index >= Length || index < 0)
                    throw new IndexOutOfRangeException();

                return index * _step + _min;
            }
        }

        public int Length
        {
            get { return Mathf.RoundToInt((_max - _min) / _step) + _isInclusive.AsInt(); }
        }

        public Enumerable Step(float step)
        {
            return new Enumerable(_min, _max, step, _isInclusive);
        }

        public Enumerable IsInclusive(bool isInclusive)
        {
            return new Enumerable(_min, _max, _step, isInclusive);
        }
    }
}
