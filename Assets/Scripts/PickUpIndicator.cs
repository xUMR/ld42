using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PickUpIndicator : MonoBehaviour
{
    private SpriteRenderer _sprite;
    private static PlayerPickUp _playerPickUp;

    private static PlayerPickUp PlayerPickUp => _playerPickUp ?? (_playerPickUp = FindObjectOfType<PlayerPickUp>());

    private void OnEnable()
    {
        _sprite = GetComponent<SpriteRenderer>();

        this.UpdateAsObservable()
            .Sample(TimeSpan.FromSeconds(.2))
            .SkipWhile(_ => PlayerPickUp == null)
            .SubscribeWithState(this, (_, self) =>
            {
                if (PlayerPickUp.ObjectInRange != self.gameObject && PlayerPickUp.PickedUpObject != self.gameObject)
                {
                    self._sprite.color = Color.gray;
                }
            })
            .AddTo(this);
    }

    public void EnterRange()
    {
        _sprite.color = Color.magenta;
    }

    public void ExitRange()
    {
        _sprite.color = Color.gray;
    }

    public void PickUp()
    {
        _sprite.color = Color.green;
    }

    public void Drop()
    {
        _sprite.color = Color.gray;
    }
}
