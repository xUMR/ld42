using System.Collections;
using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PlayerPickUp : MonoBehaviour
{
    public Rigidbody2D Rigidbody2D;
    public DistanceJoint2D Joint;

    public DoorLight Door;

    public BoxCollider2D PickUpZone;
    public LayerMask BlockingLayer;

    public float Offset;

    public bool CanPickUp;
    public bool IsRaised;
    public bool IsHoldingObject;
    public bool HasGun;

    public PlayerMove PlayerMove;
    public PlayerJump PlayerJump;
    public GameObject ObjectInRange;
    public GameObject PickedUpObject;

    private readonly RaycastHit2D[] _results = new RaycastHit2D[2];
    private readonly RaycastHit2D[] _results2 = new RaycastHit2D[2];

    private void Start()
    {
        Rigidbody2D = GetComponentInParent<Rigidbody2D>();
        Joint = GetComponentInParent<DistanceJoint2D>();
        PickUpZone = GetComponent<BoxCollider2D>();
        PlayerMove = transform.parent.GetComponentInChildren<PlayerMove>();
        PlayerJump = transform.parent.GetComponentInChildren<PlayerJump>();

        Joint.connectedBody = Rigidbody2D;

//        this.OnTriggerEnter2DAsObservable()
//            .Where(otherCollider => otherCollider.CompareTag(StringLookup.Tags.CRATE))
//            .Where(_ => !PlayerJump.IsJumping)
//            .Where(_ => PickedUpObject == null && ObjectInRange == null)
//            .SubscribeWithState(this, (otherCollider, self) =>
//            {
//                self.CanPickUp = true;
//                self.ObjectInRange = otherCollider.transform;
//                self.ObjectInRange.GetComponent<PickUpIndicator>().EnterRange();
//            })
//            .AddTo(this);
//
//        this.OnTriggerExit2DAsObservable()
//            .Where(otherCollider => otherCollider.CompareTag(StringLookup.Tags.CRATE))
//            .Where(_ => PickedUpObject == null)
//            .SubscribeWithState(this, (otherCollider, self) =>
//            {
//                self.CanPickUp = false;
//                if (otherCollider.transform != self.PickedUpObject && self.ObjectInRange != null)
//                    self.ObjectInRange.GetComponent<PickUpIndicator>().ExitRange();
//                self.ObjectInRange = null;
//            })
//            .AddTo(this);

//        this
//            .ObserveEveryValueChanged(t => t.ObjectInRange)
//            .Where(t => t != null)
//            .SubscribeWithState(this, (_, self) => self.ObjectInRange.GetComponent<PickUpIndicator>().EnterRange())
//            .AddTo(this);

        this.UpdateAsObservable()
            .SkipWhile(_ => !HasGun)
            .Where(_ => Input.GetButtonDown(StringLookup.InputAxes.FIRE_1) ||
                        Input.GetButtonDown(StringLookup.InputAxes.FIRE_3))
            .SubscribeWithState(this, (_, self) =>
            {
                if (self.Door.gameObject.activeSelf)
                {
                    print("Shoot!");
                    EventStore.instance.GameOver.OnNext(Unit.Default);
                }
                else
                {
                    print("Door is not open");
                }
            }).AddTo(this);

        this.UpdateAsObservable()
            .Where(_ => PlayerMove.IsMoving)
            .Where(_ => PickedUpObject != null)
            .SubscribeWithState(this, (_, self) =>
            {
                var otherBody = self.PickedUpObject.GetComponent<Rigidbody2D>();
                otherBody.velocity = self.Rigidbody2D.velocity;
            })
            .AddTo(this);

        this.UpdateAsObservable()
            .TakeWhile(_ => !HasGun)
            .Where(_ => Input.GetButtonDown(StringLookup.InputAxes.FIRE_3))
            .Where(_ => PickedUpObject != null || ObjectInRange != null)
            .SubscribeWithState(this, (_, self) =>
            {
                if (self.PickedUpObject != null)
                    self.Drop();
                else
                    self.PickUp();
            })
            .AddTo(this);

        this.UpdateAsObservable()
            .TakeWhile(_ => !HasGun)
            .Where(_ => Input.GetButtonDown(StringLookup.InputAxes.FIRE_1))
            .Where(_ => PickedUpObject != null)
            .SubscribeWithState(this, (_, self) =>
            {
                self.ToggleRaise();
            });

        this.FixedUpdateAsObservable()
            .TakeWhile(_ => !HasGun)
            .Where(_ => !PlayerJump.IsJumping)
            .Where(_ => PickedUpObject == null)
            .SubscribeWithState(this, (_, self) =>
            {
                var box = self.GetBoxInRange();
                if (box == null)
                {
                    if (self.ObjectInRange == null)
                        return;

                    self.ObjectInRange.GetComponent<PickUpIndicator>().ExitRange();
//                    self.ObjectInRange.GetComponent<SpriteRenderer>().color = Color.grey;
                    self.ObjectInRange = null;
                    return;
                }

                self.ObjectInRange = box;

                self.ObjectInRange.GetComponent<PickUpIndicator>().EnterRange();
//                ObjectInRange.GetComponent<SpriteRenderer>().color = Color.magenta;
            }).AddTo(this);
    }

    private GameObject GetBoxInRange()
    {
        var parent = transform.parent;
        var direction = Mathf.Sign(parent.localScale.x);
        var start = parent.position + new Vector3(direction * .325f, .5f);
        var end = start.AddY(-.75f);
        Debug.DrawLine(start, end, Color.green);
        var hits = Physics2D.LinecastNonAlloc(start, end, _results, BlockingLayer);
        if (hits < 1)
            return null;

        foreach (var hit in _results)
        {
            var otherTransform = hit.transform;
            if (otherTransform.CompareTag(StringLookup.Tags.CRATE))
            {
                start = otherTransform.position;
                end = start.AddY(.5f);
                Debug.DrawLine(start, end, Color.red);
                hits = Physics2D.BoxCastNonAlloc(start, Vector2.one * .5f, 0, Vector2.up, _results2, .3f, BlockingLayer);
//                hits = Physics2D.LinecastNonAlloc(start, end, _results2, BlockingLayer);
                if (hits > 1)
                {
//                    print($"{_results2[0].transform.name} is stack");
                    return null;
                }
                return otherTransform.gameObject;
            }
        }

        print($"GetBoxInRange: false alarm [0]:{_results[0].transform?.name} [1]:{_results[1].transform?.name}");
        return null;
    }

    public void PickUp()
    {
        PickUpZone.offset = PickUpZone.offset.WithX(0);
        CanPickUp = false;
        PickedUpObject = ObjectInRange;
        PickedUpObject.GetComponent<PickUpIndicator>().PickUp();
//        PickedUpObject.gameObject.layer = LayerMask.NameToLayer(StringLookup.Layers.IGNORE_PLAYER);
        var otherBody = PickedUpObject.GetComponent<Rigidbody2D>();
        otherBody.mass = Rigidbody2D.mass;
        otherBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
//        otherBody.gravityScale = 0;
        Joint.connectedBody = otherBody;
        Joint.enabled = true;
        otherBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        IsHoldingObject = true;
    }

    public void Drop()
    {
        var otherBody = PickedUpObject.GetComponent<Rigidbody2D>();
//        PickedUpObject.gameObject.layer = LayerMask.NameToLayer(StringLookup.Layers.DEFAULT);
        PickedUpObject.GetComponent<PickUpIndicator>().Drop();
        PickedUpObject = null;
        otherBody.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
        otherBody.mass = 1;
        otherBody.gravityScale = 9;
        Joint.enabled = false;
        otherBody.constraints = RigidbodyConstraints2D.None;
        IsHoldingObject = false;
        IsRaised = false;

        PickUpZone.offset = PickUpZone.offset.WithX(Offset);
    }

    public void ToggleRaise()
    {
//        StartCoroutine(IsRaised ? LowerCor() : RaiseCor());

        if (IsRaised)
        {
            var otherBody = PickedUpObject.GetComponent<Rigidbody2D>();
            otherBody.isKinematic = true;
            LeanTween.moveY(PickedUpObject, transform.position.y - .2f, .3f);
            otherBody.isKinematic = false;
        }
        else
        {
            var otherBody = PickedUpObject.GetComponent<Rigidbody2D>();
            otherBody.isKinematic = true;
            LeanTween.moveY(PickedUpObject, transform.position.y + .5f, .3f);
            otherBody.isKinematic = false;
        }

        IsRaised = !IsRaised;
    }

    public IEnumerator RaiseCor()
    {
        yield break;

//        var endTime = Time.time + delay;
//        while (Time.time < endTime)
//            yield return null;
    }

    public IEnumerator LowerCor()
    {
        yield break;
    }

    public void Flip()
    {

    }
}
