using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class WaitingZone : MonoBehaviour
{
    public int CrateCount;
    private FloatRange _spawnDelay;
    private int _waitForCrateCount;

    private void OnEnable()
    {
        _spawnDelay = FloatRange.Of(.8f, 1.6f);

        _waitForCrateCount = Mathf.RoundToInt(UnityEngine.Random.value * 4 + 1);

        this.UpdateAsObservable()
            .Sample(TimeSpan.FromSeconds(5))
            .SubscribeWithState(this, (_, self) =>
            {
                if (self.CrateCount > self._waitForCrateCount)
                    return;

                EventStore.instance.DirectorSendCrate.OnNext(self._spawnDelay.Random());
            })
            .AddTo(this);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.CRATE))
        {
            CrateCount++;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.CRATE))
        {
            CrateCount--;
        }
    }
}
