using Extensions;
using UnityEngine;

public class PartyGoerRandomize : MonoBehaviour
{
    public FloatRange ScaleX;
    public FloatRange ScaleY;

    public FloatRange PositionX;

    private void OnEnable()
    {
        transform.localPosition = transform.localPosition.AddX(PositionX.Random());

        var scaleX = ScaleX.Random();
        var scaleY = ScaleY.Random();
        transform.localScale = transform.localScale.WithXY(scaleX, scaleY);

        FixPosition(transform);

        GetComponent<SpriteRenderer>().color = Random.ColorHSV(0, 1);
    }

    public static void FixPosition(Transform transform)
    {
        var scaleY = transform.localScale.y;
        transform.localPosition = transform.localPosition.WithY((scaleY - 1) / 2);
    }
}
