using System.Runtime.Serialization.Formatters;
using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Inputs Inputs;

    [HideInInspector]
    public Rigidbody2D Rigidbody2D;

    private TextDisplay _textDisplay;

    private PlayerPickUp _playerPickUp;

    public float Speed;

    public LayerMask BlockingLayer;

    private Subject<bool> _isMovingChanged;
    public bool IsMoving;
    public bool IsBlocked;

    private readonly RaycastHit2D[] _results = new RaycastHit2D[2];

    private SpriteRenderer _sprite;

    private float _scaleX;

    private float _lastMovementStoppedTime;
    private float _waitTimeForDriftOffMsg;
    private bool _hasMoved;

    private void Start()
    {
        Rigidbody2D = GetComponentInParent<Rigidbody2D>();
        _playerPickUp = transform.parent.GetComponentInChildren<PlayerPickUp>();
        _sprite = transform.parent.GetComponentInChildren<SpriteRenderer>();
        _textDisplay = transform.parent.GetComponentInChildren<TextDisplay>();

        _scaleX = transform.parent.localScale.x;
        _isMovingChanged = new Subject<bool>();

        _waitTimeForDriftOffMsg = 15;
        _lastMovementStoppedTime = float.MaxValue;

        var onMovement = Inputs.Movement
            .Where(inputMovement => Rigidbody2D.velocity != Vector2.zero || inputMovement != Vector2.zero);

        onMovement
            .SubscribeWithState(this, (v, self) =>
            {
                if (!self._hasMoved && v != Vector2.zero)
                {
                    _hasMoved = true;
                }

                var movement = self.transform.right * v.x * self.Speed;
                self.Rigidbody2D.velocity = self.Rigidbody2D.velocity.WithX(movement.x);

                self.IsMoving = v != Vector2.zero;
                if (self._hasMoved)
                    self._isMovingChanged.OnNext(self.IsMoving);
            })
            .AddTo(this);

        _isMovingChanged
            .SubscribeWithState(this, (value, self) =>
            {
                if (!value)
                {
                    self._lastMovementStoppedTime = Time.time;
                }
                else if (Time.time - self._lastMovementStoppedTime > _waitTimeForDriftOffMsg)
                {
                    _waitTimeForDriftOffMsg += 15;
                    self._textDisplay.FadeInOutToUp("Oh! I must have drifted off...".Italic(),
                        0, .3f, 1.4f, .3f);

                    self._textDisplay.FadeInOutToUp("I'd better get back to work.".Italic(),
                        1.8f, .3f, 1.5f, .3f);
                }
            })
            .AddTo(this);

        onMovement
            .Where(_ => !_playerPickUp.IsHoldingObject)
            .SubscribeWithState(this, (v, self) =>
            {
                var scale = self.transform.parent.transform.localScale;

                if (v.x < -float.Epsilon)
                    self.transform.parent.transform.localScale = scale.WithX(-self._scaleX);
                else if (v.x > float.Epsilon)
                    self.transform.parent.transform.localScale = scale.WithX(self._scaleX);
            })
            .AddTo(this);

        this.FixedUpdateAsObservable()
            .SubscribeWithState(this, (_, self) =>
            {
                if (!self.IsMoving)
                {
                    self.IsBlocked = false;
                    return;
                }
                var start = self.transform.parent.position;
                var offset = Mathf.Sign(self.Rigidbody2D.velocity.x) * .25f;
                var end = start.AddX(offset);
                Debug.DrawLine(start, end, Color.magenta);
                var hitCount = Physics2D.LinecastNonAlloc(start, end, self._results, self.BlockingLayer);
                self.IsBlocked = hitCount > 0;
                self.IsMoving = !self.IsBlocked && self.IsMoving;
            })
            .AddTo(this);
    }
}
