using System;
using System.Collections;
using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public AudioClip Sfx;

    public bool IsPickedUp;
    public Transform Door;

    private Transform _bullet;

    private void OnEnable()
    {
        _bullet = transform.GetChild(0);

        this.UpdateAsObservable()
            .SkipWhile(_ => !IsPickedUp)
            .Sample(TimeSpan.FromSeconds(.25))
            .SubscribeWithState(this, (_, self) => self.LookAtDoor())
            .AddTo(this);

        EventStore.instance.GameOver
            .SubscribeWithState(this, (_, self) => self.Shoot()).AddTo(this);
    }

    private void LookAtDoor()
    {
        var d = Door.position - transform.position;
        var angle = Mathf.Rad2Deg * Mathf.Atan2(d.y, d.x);
        transform.rotation = Quaternion.Euler(0, 0, angle);

        _bullet.localEulerAngles = Vector3.zero.WithZ(180 + angle);
    }

    public void Shoot()
    {
        if (!IsPickedUp)
            return;

        LeanAudio.play(Sfx);
        StartCoroutine(ShoorCoroutine());
    }

    private IEnumerator ShoorCoroutine()
    {
        var v = (Door.position - _bullet.position).normalized * .1f;
        v = v.MultiplyX(-1);

        var endTime = Time.unscaledTime + 2;
        while (Time.unscaledTime < endTime)
        {
            _bullet.transform.Translate(v);
//            _bullet.transform.SetPositionX(_bullet.transform.GetPositionX() + Time.deltaTime);
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.PLAYER))
        {
            GetComponent<Rotator>().enabled = false;
            transform.rotation = Quaternion.identity;
            transform.parent = other.transform;
            transform.localScale = transform.localScale.MultiplyXY(-.33f, -.33f);
            GetComponent<BoxCollider2D>().enabled = false;

            other.GetComponentInChildren<PlayerPickUp>().HasGun = true;
            IsPickedUp = true;

            EventStore.instance.GunIsPickedUp.OnNext(Unit.Default);
        }
    }
}
