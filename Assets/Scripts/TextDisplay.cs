using System.Collections;
using Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplay : MonoBehaviour
{
    public Text[] Texts;
    private bool[] _isBusy;

    private void OnEnable()
    {
        _isBusy = new bool[Texts.Length];

        foreach (var text in Texts)
            text.color = Color.clear;
    }

    private static void HideText(Text text)
    {
        text.color = Color.white.WithAlpha(0);
    }

    private Text AvailableText
    {
        get
        {
            for (var i = _isBusy.Length - 1; i >= 0; i--)
            {
                if (!_isBusy[i])
                {
                    _isBusy[i] = true;
                    return Texts[i];
                }
            }
            Debug.LogWarning("No 'Text's available!");
            return null;
        }
    }

    private void FreeTextInstance(Text text)
    {
        for (var i = Texts.Length - 1; i >= 0; i--)
            if (text == Texts[i])
                _isBusy[i] = false;
    }

    public void FadeInOutToRight(
        string message,
        float delay,
        float appearDuration,
        float stayDuration,
        float disappearDuration)
    {
        StartCoroutine(FadeInOutCoroutine(message, delay, appearDuration, stayDuration, disappearDuration, true));
    }

    public void FadeInOutToUp(
        string message,
        float delay,
        float appearDuration,
        float stayDuration,
        float disappearDuration)
    {
        StartCoroutine(FadeInOutCoroutine(message, delay, appearDuration, stayDuration, disappearDuration, false));
    }

    private IEnumerator FadeInOutCoroutine(string message, float delay, float appearDuration, float stayDuration, float disappearDuration, bool toRight)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
            yield return null;

        var text = AvailableText;
        if (text == null)
            yield break;
        var prevRectPosition = text.rectTransform.position;
        var prevPosition = text.transform.position;

        HideText(text);
        text.text = message;
//        var prevScale = text.rectTransform.localScale;

        var delta = toRight.Select(new Vector2(60, 10), new Vector2(0, 40));
        LeanTween.moveX(text.gameObject, text.transform.GetPositionX() + delta.x, appearDuration + stayDuration).setEaseOutCubic();
        LeanTween.moveY(text.gameObject, text.transform.GetPositionY() + delta.y, appearDuration + stayDuration).setEaseInCubic();
//        LeanTween.scaleY(text.gameObject, 0, (appearDuration + stayDuration + disappearDuration) * 8);

        LeanTween.textColor(text.rectTransform, Color.white, appearDuration);
        LeanTween.textAlpha(text.rectTransform, 1, appearDuration);
        LeanTween.textAlpha(text.rectTransform, 0, disappearDuration).setDelay(stayDuration);

        var totalDuration = appearDuration + stayDuration + disappearDuration;

        endTime = Time.time + totalDuration;
        while (Time.time < endTime)
            yield return null;

        text.rectTransform.position = prevRectPosition;
        text.transform.position = prevPosition;

        FreeTextInstance(text);
    }
}
