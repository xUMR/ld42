using System;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Inputs : MonoBehaviour
{
    public IObservable<Vector2> Movement;
    public IObservable<Unit> Jump;

//    public Dictionary<string, ReadOnlyReactiveProperty<bool>> ButtonDown { get; private set; }
//    public Dictionary<string, ReadOnlyReactiveProperty<bool>> ButtonUp { get; private set; }
//    public Dictionary<string, ReadOnlyReactiveProperty<bool>> Button { get; private set; }

    private void OnEnable()
    {
        PrepareButtons();

        Movement = this.UpdateAsObservable().Select(_ =>
        {
            var x = Input.GetAxisRaw(StringLookup.InputAxes.HORIZONTAL);
            var y = Input.GetAxisRaw(StringLookup.InputAxes.VERTICAL);
            return new Vector2(x, y).normalized;
        });

        Jump = this.UpdateAsObservable().Where(_ => Input.GetButtonDown(StringLookup.InputAxes.JUMP));
    }

    private void PrepareButtons()
    {
//        ButtonDown = new Dictionary<string, ReadOnlyReactiveProperty<bool>>();
//        ButtonUp = new Dictionary<string, ReadOnlyReactiveProperty<bool>>();
//        Button = new Dictionary<string, ReadOnlyReactiveProperty<bool>>();
//
//        foreach (var button in InputAxes.AllInputAxesArray)
//        {
//            ButtonDown[button] = this.UpdateAsObservable()
//                .Select(_ => Input.GetButtonDown(button))
//                .ToReadOnlyReactiveProperty()
//                .AddTo(this);
//
//            ButtonUp[button] = this.UpdateAsObservable()
//                .Select(_ => Input.GetButtonUp(button))
//                .ToReadOnlyReactiveProperty()
//                .AddTo(this);
//
//            Button[button] = this.UpdateAsObservable()
//                .Select(_ => Input.GetButton(button))
//                .ToReadOnlyReactiveProperty()
//                .AddTo(this);
//        }
    }
}
