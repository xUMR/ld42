public static class InputAxes
{
    public const string Horizontal = "Horizontal";
    public const string Vertical = "Vertical";
    public const string Fire1 = "Fire1"; // default left ctrl
    public const string Fire2 = "Fire2"; // default left alt
    public const string Fire3 = "Fire3"; // default left shift
    public const string Jump = "Jump"; // default space
    public const string Submit = "Submit"; // default enter
    public const string Cancel = "Cancel"; // default esc

    public static string[] AllInputAxesArray =
    {
        Horizontal,
        Vertical,
        Fire1,
        Fire2,
        Fire3,
        Jump,
        Submit,
        Cancel
    };
}
