using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdater : MonoBehaviour
{
    public ZoneScore[] Zones;
    public Text Text;

    private void Awake()
    {
        this.UpdateAsObservable()
            .Sample(TimeSpan.FromSeconds(.1))
            .SubscribeWithState(this, (_, self) =>
            {
                var score = Mathf.RoundToInt(self.Zones.Average(zone => zone.Score) * 100);
                self.Text.text = $"CRATE STACKING SCORE: {score}%";
            })
            .AddTo(this);
    }
}
