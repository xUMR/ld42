using UnityEngine;

public class SecretWall : MonoBehaviour
{
    private SpriteRenderer _renderer;

    private void OnEnable()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.PLAYER))
        {
            LeanTween.color(gameObject, _renderer.color.WithAlpha(.25f), .3f).setEaseInCubic();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(StringLookup.Tags.PLAYER))
        {
            LeanTween.color(gameObject, _renderer.color.WithAlpha(1), .3f).setEaseInCubic();
        }
    }
}
