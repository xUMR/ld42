using System;
using System.Collections;
using System.Linq;
using Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    public SceneFader SceneFader;
    public TextDisplay TextDisplay;
    public BoxSpawner BoxSpawner;
    public DoorLight DoorLight;
    public Gun Gun;

    public string[] Messages;
    private int _msgIndex;

    private FloatRange CheckOnPeriod;

    private void OnEnable()
    {
        var eventStore = EventStore.instance;

        eventStore.WakeUp
            .SubscribeWithState(this, (_, self) =>
            {
                self.SceneFader.FadeToBlack(0, 0);
                self.SceneFader.FadeFromBlack(.6f, 1.25f);
                self.SceneFader.FadeToBlack(.5f, 1.6f);
                self.SceneFader.FadeFromBlack(.3f, 2f);
            })
            .AddTo(this);

        eventStore.DirectorYellWakeUp
            .SubscribeWithState(this, (delay, self) =>
            {
                var message = "WAKE UP!".Bold().Italic();
                self.TextDisplay.FadeInOutToRight(message, delay, 0.3f, 1.8f, 0.3f);

                message = "AND GET BACK TO WORK!".Bold().Italic();
                self.TextDisplay.FadeInOutToRight(message, 1.7f, 0.3f, 1.8f, 0.3f);
            })
            .AddTo(this);

        eventStore.DirectorYellWork
            .SubscribeWithState(this, (delay, self) =>
            {
                var message = "GET BACK TO WORK!".Bold().Italic();
                self.TextDisplay.FadeInOutToRight(message, delay, 0.3f, 1.8f, 0.3f);
            })
            .AddTo(this);

        eventStore.DirectorSendCrate
            .SubscribeWithState(this, (delay, self) =>
            {
                self.DoorLight.HalfDuration = delay;
                self.DoorLight.gameObject.SetActive(true);

                self.BoxSpawner.SpawnWithForce(delay);
            })
            .AddTo(this);

        eventStore.DirectorYell
            .SubscribeWithState(this, (delay, self) =>
            {
                self.DoorLight.HalfDuration = delay;
                self.DoorLight.gameObject.SetActive(true);

                self._msgIndex = (self._msgIndex + 1) % self.Messages.Length;
                var message = self.Messages[self._msgIndex].Bold().Italic();
                self.TextDisplay.FadeInOutToRight(message, delay, 0.3f, delay, 0.3f);
            })
            .AddTo(this);

        eventStore.DirectorCheckOn
            .SubscribeWithState(this, (_, self) =>
            {
                self.DoorLight.HalfDuration = 1;
                self.DoorLight.gameObject.SetActive(true);

                if (UnityEngine.Random.value > .5f)
                {
                    self._msgIndex = (self._msgIndex + 1) % self.Messages.Length;
                    var message = self.Messages[self._msgIndex].Bold().Italic();
                    self.TextDisplay.FadeInOutToRight(message, 1, 0.3f, 1.6f, 0.3f);
                }
            })
            .AddTo(this);

        eventStore.GameOver.SubscribeWithState(this, (_, self) =>
        {
            self.StartCoroutine(self.SlowTimeCor());

            self.SceneFader.FadeToBlack(.25f, .5f);


        });

        eventStore.GunIsPickedUp
            .SubscribeWithState(this, (_, self) =>
            {
                EventStore.instance.DirectorCheckOn.OnCompleted();
                EventStore.instance.DirectorSendCrate.OnCompleted();
                EventStore.instance.DirectorYell.OnCompleted();
                EventStore.instance.DirectorYellWakeUp.OnCompleted();
                EventStore.instance.DirectorYellWork.OnCompleted();

                var message = "NO! DON'T!".Bold().Italic();
                self.TextDisplay.FadeInOutToRight(message, .8f, 0.3f, 1, 0.3f);
            })
            .AddTo(this);

        Messages = Messages.Shuffle().ToArray();
        CheckOnPeriod = FloatRange.Of(15, 90);
        StartCoroutine(DirectorCheckOnCor(CheckOnPeriod.Avg));
//        StartCoroutine(DirectorYellCor());

//        SceneFader.FadeToBlack(0, 0);
//        SceneFader.FadeFromBlack(.6f, 1.25f);
//        SceneFader.FadeToBlack(.5f, 1.6f);
//        SceneFader.FadeFromBlack(.3f, 2f);
//
//        DoorLight.HalfDuration = 1.6f;
//        DoorLight.gameObject.SetActive(true);
//
//        var message = "WAKE UP!".Bold().Italic();
//        TextDisplay.FadeInOut(message, .2f, 0.3f, 1.1f, 0.3f);
//
//        message = "AND GET BACK TO WORK!".Bold().Italic();
//        TextDisplay.FadeInOut(message, 1.7f, 0.3f, 1.8f, 0.3f);
//
//        BoxSpawner.SpawnWithForce(1.6f);
    }

    private IEnumerator DirectorCheckOnCor(float delay)
    {
        while (isActiveAndEnabled)
        {
            var random = (FloatRange.MinusOneOne * CheckOnPeriod.HalfRange).Random();

            var endTime = Time.time + delay + random;
            while (Time.time < endTime)
                yield return null;

            EventStore.instance.DirectorCheckOn.OnNext(Unit.Default);
        }
    }

    private IEnumerator SlowTimeCor()
    {
//        var endTime = Time.unscaledTime + 1;
        while (Time.timeScale > .1f)
        {
            Time.timeScale -= .03f;
            yield return null;
        }
        Time.timeScale = .1f;

        var image = SceneFader.Panel.GetComponent<Image>();
        while (image.color.a < .9f)
        {
            yield return null;
        }

        Time.timeScale = 1;
    }

    private IEnumerator DirectorYellCor()
    {
        var yellPeriodRange = FloatRange.Of(20, 80);
        var yellDurationRange = FloatRange.Of(1.2f, 2);

        while (isActiveAndEnabled)
        {
            var endTime = Time.time + yellPeriodRange.Random();
            while (Time.time < endTime)
                yield return null;

            EventStore.instance.DirectorYell.OnNext(yellDurationRange.Random());
        }
    }

    private void Start()
    {
        EventStore.instance.DirectorCheckOn.OnNext(Unit.Default);
        EventStore.instance.DirectorYellWakeUp.OnNext(.2f);
        EventStore.instance.WakeUp.OnNext(0);
        EventStore.instance.DirectorSendCrate.OnNext(1.6f);
    }
}
