using System.Collections;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    public float FlickerLength;
    public FloatRange FlickerPeriod;
    public Light Light;

    private void OnEnable()
    {
        Light = GetComponent<Light>();

        StartCoroutine(FlickerCor(FlickerPeriod.Avg));
    }

    private IEnumerator FlickerCor(float delay)
    {
        while (isActiveAndEnabled)
        {
            var random = (FloatRange.MinusOneOne * FlickerPeriod.HalfRange).Random();

            var endTime = Time.time + delay + random;
            while (Time.time < endTime)
                yield return null;

            var actualIntensity = Light.intensity;

            for (var i = 0; i < FlickerLength; i++)
            {
                Light.intensity = (FloatRange.ZeroOne * actualIntensity).Random();
                yield return null;
            }

            Light.intensity = actualIntensity;
        }
    }
}
