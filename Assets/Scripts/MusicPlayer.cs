using System.Collections;
using UniRx;
using UnityEngine;

public class MusicPlayer : MonoSingletonExt<MusicPlayer>
{
//    public AudioClip Intro;
    public AudioClip Loop;

//    private AudioSource _audioIntro;
    private AudioSource _audioLoop;

    private void Start()
    {
        var delay = .25f;

//        _audioIntro = gameObject.AddComponent<AudioSource>();
//        _audioIntro.clip = Intro;
//        _audioIntro.PlayDelayed(delay);

        _audioLoop = gameObject.AddComponent<AudioSource>();
        _audioLoop.clip = Loop;
        _audioLoop.loop = true;
//        _audioLoop.PlayDelayed(Intro.length + delay);
        _audioLoop.PlayDelayed(delay);

        EventStore.instance.GameOver
            .SubscribeWithState(this, (_, self) =>
            {
                StartCoroutine(self.StopMusicCoroutine());
            })
            .AddTo(this);
    }

    private IEnumerator StopMusicCoroutine()
    {
        var endTime = Time.unscaledTime + 2;
        while (Time.unscaledTime < endTime)
        {
//            _audioIntro.volume -= .05f;
            _audioLoop.volume -= .005f;
            _audioLoop.pitch -= .001f;
            yield return null;
        }
    }
}
