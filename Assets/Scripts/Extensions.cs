﻿using UnityEngine;

namespace Extensions
{
    public static class MyExtensions
    {
        #region float & int methods

        public static T SignSelect<T>(this int n, T whenNegative, T whenZero, T whenPositive)
        {
            if (n == 0) return whenZero;
            return n > 0 ? whenPositive : whenNegative;
        }

        public static bool IsNonPositive(this float f)
        {
            return f < float.Epsilon;
        }

        public static bool IsNonNegative(this float f)
        {
            return f > -float.Epsilon;
        }

        public static bool IsZero(this float f)
        {
            return Mathf.Approximately(f, 0);
        }

        public static bool IsNonZero(this float f)
        {
            return !Mathf.Approximately(f, 0);
        }

        public static bool Approx(this float f, float other)
        {
            return Mathf.Approximately(f, other);
        }

        public static bool LessOrApprox(this float f, float other)
        {
            return Mathf.Approximately(f, other) || f < other;
        }

        public static bool GreaterOrApprox(this float f, float other)
        {
            return Mathf.Approximately(f, other) || f > other;
        }

        public static float RoundToNearestMultipleOf(this float f, float k)
        {
            var m = f % k;
            if (m < k / 2) return f - m;
            return f + k - m;
        }

        public static float RoundToNearestMultipleOf(this int n, int k)
        {
            var m = n % k;
            if (m < k / 2) return n - m;
            return n + k - m;
        }

        #endregion

        #region Vector2 methods

        public static float Random(this Vector2 v)
        {
            return UnityEngine.Random.Range(v.x, v.y);
        }

        public static Vector2 WithX(this Vector2 v, float x)
        {
            return new Vector2(x, v.y);
        }

        public static Vector2 WithY(this Vector2 v, float y)
        {
            return new Vector2(v.x, y);
        }

        public static Vector3 WithZ(this Vector2 v, float z)
        {
            return new Vector3(v.x, v.y, z);
        }

        public static Vector2 AddX(this Vector2 v, float x)
        {
            return new Vector2(v.x + x, v.y);
        }

        public static Vector2 AddY(this Vector2 v, float y)
        {
            return new Vector2(v.x, v.y + y);
        }

        public static Vector2 RotateAround(this Vector2 point, Vector2 pivot, float zAngle)
        {
            return (Quaternion.Euler(0, 0, zAngle) * (point - pivot)).XY() + pivot;
        }

        public static Vector2 RotateAround(this Vector2 point, Vector2 pivot, Vector3 angles)
        {
            return (Quaternion.Euler(angles) * (point - pivot)).XY() + pivot;
        }

        public static float DistanceX(this Vector2 v, Vector2 t)
        {
            return Mathf.Abs(v.x - t.x);
        }

        public static float DistanceY(this Vector2 v, Vector2 t)
        {
            return Mathf.Abs(v.y - t.y);
        }

        public static bool Approx(this Vector2 v, Vector2 t)
        {
            return Mathf.Approximately(v.x, t.x) && Mathf.Approximately(v.y, t.y);
        }

        public static float Area(this Vector2 v)
        {
            return v.x * v.y;
        }

        #endregion

        #region Vector3 methods

        public static Vector3 WithX(this Vector3 v, float x)
        {
            return new Vector3(x, v.y, v.z);
        }

        public static Vector3 WithY(this Vector3 v, float y)
        {
            return new Vector3(v.x, y, v.z);
        }

        public static Vector3 WithZ(this Vector3 v, float z)
        {
            return new Vector3(v.x, v.y, z);
        }

        public static Vector3 WithXY(this Vector3 v, float x, float y)
        {
            return new Vector3(x, y, v.z);
        }

        public static Vector3 WithXY(this Vector3 v, Vector2 t)
        {
            return new Vector3(t.x, t.y, v.z);
        }

        public static Vector3 AddX(this Vector3 v, float x)
        {
            return new Vector3(v.x + x, v.y, v.z);
        }

        public static Vector3 AddY(this Vector3 v, float y)
        {
            return new Vector3(v.x, v.y + y, v.z);
        }

        public static Vector3 AddXY(this Vector3 v, float x, float y)
        {
            return new Vector3(v.x + x, v.y + y, v.z);
        }

        public static Vector3 AddXY(this Vector3 v, Vector2 xy)
        {
            return new Vector3(v.x + xy.x, v.y + xy.y, v.z);
        }

        public static Vector3 AddZ(this Vector3 v, float z)
        {
            return new Vector3(v.x, v.y, v.z + z);
        }

        public static Vector3 MultiplyX(this Vector3 v, float x)
        {
            return new Vector3(v.x * x, v.y, v.z);
        }

        public static Vector3 MultiplyY(this Vector3 v, float y)
        {
            return new Vector3(v.x, v.y * y, v.z);
        }

        public static Vector3 MultiplyZ(this Vector3 v, float z)
        {
            return new Vector3(v.x, v.y, v.z * z);
        }

        public static Vector3 MultiplyXY(this Vector3 v, float x, float y)
        {
            return new Vector3(v.x * x, v.y * y, v.z);
        }

        public static Vector2 XY(this Vector3 v, Vector2 offset)
        {
            return new Vector2(v.x + offset.x, v.y + offset.y);
        }

        public static Vector2 XY(this Vector3 v, float x = 0, float y = 0)
        {
            return new Vector2(v.x + x, v.y + y);
        }

        public static Vector2 XZ(this Vector3 v, float x = 0, float z = 0)
        {
            return new Vector2(v.x + x, v.z + z);
        }

        public static Vector2 YZ(this Vector3 v, float y = 0, float z = 0)
        {
            return new Vector2(v.y + y, v.z + z);
        }

        public static Vector3 RotateAround(this Vector3 point, Vector3 pivot, float zAngle)
        {
            return (Quaternion.Euler(0, 0, zAngle) * (point - pivot)) + pivot;
        }

        public static float DistanceX(this Vector3 v, Vector3 t)
        {
            return Mathf.Abs(v.x - t.x);
        }

        public static float DistanceY(this Vector3 v, Vector3 t)
        {
            return Mathf.Abs(v.y - t.y);
        }

        public static float DistanceZ(this Vector3 v, Vector3 t)
        {
            return Mathf.Abs(v.z - t.z);
        }

        public static bool ApproxXY(this Vector3 v, Vector2 t)
        {
            return Mathf.Approximately(v.x, t.x) && Mathf.Approximately(v.y, t.y);
        }

        public static bool Approx(this Vector3 v, Vector3 t)
        {
            return Mathf.Approximately(v.x, t.x) && Mathf.Approximately(v.y, t.y) && Mathf.Approximately(v.z, t.z);
        }

        #endregion

        #region Color methods

        public static Color WithRed(this Color color, float r)
        {
            return new Color(r, color.g, color.b, color.a);
        }

        public static Color WithGreen(this Color color, float g)
        {
            return new Color(color.r, g, color.b, color.a);
        }

        public static Color WithBlue(this Color color, float b)
        {
            return new Color(color.r, color.g, b, color.a);
        }

        public static Color WithAlpha(this Color color, float a)
        {
            return new Color(color.r, color.g, color.b, a);
        }

        public static Color AddAlpha(this Color color, float a)
        {
            return new Color(color.r, color.g, color.b, color.a + a);
        }

        public static Color AddRgbOffset(this Color color, float offset)
        {
            return new Color(color.r + offset, color.g + offset, color.b + offset);
        }

        public static bool ApproxRgb(this  Color c, Color t)
        {
            return Mathf.Approximately(c.r, t.r) && Mathf.Approximately(c.g, t.g) && Mathf.Approximately(c.b, t.b);
        }

        public static bool ApproxRgba(this  Color c, Color t)
        {
            return Mathf.Approximately(c.r, t.r) && Mathf.Approximately(c.g, t.g) && Mathf.Approximately(c.b, t.b) &&
                   Mathf.Approximately(c.a, t.a);
        }

        #endregion

        #region bool methods
        public static int AsInt(this bool b, int whenTrue = 1, int whenFalse = 0)
        {
            return b ? whenTrue : whenFalse;
        }

        public static Color AsColor(this bool b, Color whenTrue, Color whenFalse)
        {
            return b ? whenTrue : whenFalse;
        }

        public static T Select<T>(this bool b, T whenTrue, T whenFalse)
        {
            return b ? whenTrue : whenFalse;
        }
        #endregion

        public static Transform GetRootTransform(this Component self)
        {
            while (true)
            {
                var parent = self.transform.parent;
                if (parent == null)
                    return self.transform;
                self = parent;
            }
        }

        public static Transform GetRootTransform(this GameObject self)
        {
            return self.transform.GetRootTransform();
        }

        public static string Bold(this string text)
        {
            return $"<b>{text}</b>";
        }

        public static string Italic(this string text)
        {
            return $"<i>{text}</i>";
        }
    }
}
