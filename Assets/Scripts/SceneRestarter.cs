using UniRx;
#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneRestarter : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            RestartScene();
            ClearConsole();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            EventStore.instance.GameOver.OnNext(Unit.Default);
        }
    }

    public static void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ClearConsole()
    {
        #if UNITY_EDITOR
        Assembly.GetAssembly(typeof(Editor))
            ?.GetType("UnityEditor.LogEntries")
            ?.GetMethod("Clear")
            ?.Invoke(new object(), null);
        #endif
    }
}
