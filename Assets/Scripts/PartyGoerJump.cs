using System.Collections;
using UnityEngine;

public class PartyGoerJump : MonoBehaviour
{
    public FloatRange JumpDelayRange;
    public float ActivationProbability;
    public float Magnitude;

    private void OnEnable()
    {
        Magnitude = .3f;
        if (ActivationProbability < Random.value)
            enabled = false;

        StartCoroutine(JumpCor(JumpDelayRange.Random()));
//        LeanTween.moveLocalY(gameObject, transform.localPosition.y + 1, 1).setEaseOutQuint();
    }

    private IEnumerator JumpCor(float delay)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
            yield return null;

        while (isActiveAndEnabled)
        {
            var prevPosY = transform.localPosition.y;
            LeanTween.moveLocalY(gameObject, transform.localPosition.y + Magnitude, .25f);
            LeanTween.moveLocalY(gameObject, prevPosY, .25f).setDelay(.25f);

            var deltaX = (FloatRange.MinusOneOne * .01f).Random();
            LeanTween.moveLocalX(gameObject, transform.localPosition.x + deltaX, .5f);

            endTime = Time.time + .5f;
            while (Time.time < endTime)
                yield return null;

            PartyGoerRandomize.FixPosition(transform);
        }
    }
}
