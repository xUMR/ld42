using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    public Text[] Texts;
    public ZoneScore[] Zones;

    private void Awake()
    {
        EventStore.instance.GameOver
            .SubscribeWithState(this, (_, self) =>
            {
                var score = Mathf.RoundToInt(self.Zones.Average(zone => zone.Score) * 100);
                self.Texts[1].text = $"CRATE STACKING SCORE: {score}%";

                foreach (var text in self.Texts)
                    LeanTween.textAlpha(text.rectTransform, 1, 1f).setDelay(2);

                foreach (var text in self.Texts)
                    LeanTween.textAlpha(text.rectTransform, 0, 1f).setDelay(5);
            })
            .AddTo(this);
    }
}
