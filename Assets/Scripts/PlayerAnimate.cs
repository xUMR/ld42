using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PlayerAnimate : MonoBehaviour
{
    public Rigidbody2D Rigidbody2D;
    public PlayerMove PlayerMove;
    public SpriteRenderer SpriteRenderer;

    public float Multiplier;
    public float Movement;
    private float _limitUp;
    private float _limitDown;

    private float _movement = 1;

    private Transform _spriteTransform;

    private void Start()
    {
        Rigidbody2D = GetComponentInParent<Rigidbody2D>();
        PlayerMove = transform.parent.GetComponentInChildren<PlayerMove>();
        SpriteRenderer = GetComponent<SpriteRenderer>();

        _spriteTransform = SpriteRenderer.transform;

        _limitUp = SpriteRenderer.transform.localPosition.y + Movement / 2;
        _limitDown = SpriteRenderer.transform.localPosition.y - Movement / 2;

        this.UpdateAsObservable()
            .SubscribeWithState(this, (_, self) =>
            {
//                if (self._spriteTransform.localPosition.y > self._limitUp) { }
//                self.SpriteRenderer.transform.Translate();
            })
            .AddTo(this);

//        LeanTween.moveLocalY(SpriteRenderer.gameObject, SpriteRenderer.transform.localPosition.y + Movement, 1)
//            .setLoopPingPong();
    }

    private void Update()
    {
        if (PlayerMove.IsBlocked || !PlayerMove.IsMoving)
        {
            _spriteTransform.localPosition = _spriteTransform.localPosition.WithY(0);
            return;
        }

        if (_spriteTransform.localPosition.y > _limitUp)
            _movement = -1;
        else if (_spriteTransform.localPosition.y < _limitDown)
            _movement = 1;

        var v = Vector3.up * Multiplier * _movement * Time.deltaTime * Rigidbody2D.velocity.sqrMagnitude;
        _spriteTransform.Translate(v);
    }
}
