﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class CustomStringsSO : ScriptableObject
{
	public List<string> CustomStrings = new List<string>();
}
