﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class StringLookupSettings
{
	//	TODO:	Turn into a SO and implement option for white list folders so only specific folders are searched
	public const string NAMESPACE = "UnityEngine";

	public const string RESOURCES_FILE_PATH = "Assets/StringLookup/Resources/";
	public const string SCRIPT_FILE_PATH = "Assets/StringLookup/Scripts/";
	public const string LOOKUP_FOLDER = "Lookup/";

	public const string PRIMARY_CLASS = "StringLookup";

	public const string CUSTOM_STRING_SO = "CustomStrings";
	public const string CUSTOM_STRING_ASSET = "CustomStrings.asset";
}
