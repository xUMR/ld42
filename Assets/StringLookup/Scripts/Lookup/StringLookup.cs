namespace UnityEngine
{
	public static class StringLookup
	{
		
		/* DO NOT MODIFY - UPDATING OVERWRITES */
		
		public static class Tags
		{
			public const string UNTAGGED = "Untagged";
			public const string RESPAWN = "Respawn";
			public const string FINISH = "Finish";
			public const string EDITOR_ONLY = "EditorOnly";
			public const string MAIN_CAMERA = "MainCamera";
			public const string PLAYER = "Player";
			public const string GAME_CONTROLLER = "GameController";
			public const string CRATE = "Crate";
			public const string FLOOR = "Floor";
		}

		public static class Layers
		{
			public const string DEFAULT = "Default";
			public const string TRANSPARENT_FX = "TransparentFX";
			public const string IGNORE_RAYCAST = "Ignore Raycast";
			public const string WATER = "Water";
			public const string UI = "UI";
			public const string PLAYER = "Player";
			public const string IGNORE_PLAYER = "IgnorePlayer";
			public const string NEW_CRATE = "NewCrate";
			public const string WALL = "Wall";
			public const string RAMP = "Ramp";
			public const string ZONE = "Zone";
		}

		public static class SortingLayers
		{
			public const string DEFAULT = "Default";
		}

		public static class InputAxes
		{
			public const string HORIZONTAL = "Horizontal";
			public const string VERTICAL = "Vertical";
			public const string FIRE_1 = "Fire1";
			public const string FIRE_2 = "Fire2";
			public const string FIRE_3 = "Fire3";
			public const string JUMP = "Jump";
			public const string MOUSE_X = "Mouse X";
			public const string MOUSE_Y = "Mouse Y";
			public const string MOUSE_SCROLL_WHEEL = "Mouse ScrollWheel";
			public const string DUPLICATE_HORIZONTAL = "Horizontal";
			public const string DUPLICATE_VERTICAL = "Vertical";
			public const string DUPLICATE_FIRE_1 = "Fire1";
			public const string DUPLICATE_FIRE_2 = "Fire2";
			public const string DUPLICATE_FIRE_3 = "Fire3";
			public const string DUPLICATE_JUMP = "Jump";
			public const string SUBMIT = "Submit";
			public const string DUPLICATE_SUBMIT = "Submit";
			public const string CANCEL = "Cancel";
		}

		public static class Audio
		{
		}

		public static class Prefabs
		{
			public const string SAMPLE_13_TO_DO_ITEM = "Sample13_ToDoItem";
			public const string BOX = "Box";
			public const string BOX_SPAWNER = "BoxSpawner";
			public const string FLY = "Fly";
			public const string LAMP = "Lamp";
			public const string PARTY_GOER = "PartyGoer";
			public const string PARTY_GOER_SPAWNER = "PartyGoerSpawner";
			public const string PLAYER = "Player";
			public const string SPOT_LIGHT_2 = "Spot Light (2)";
		}

		public static class Scenes
		{
		}

		public static class Materials
		{
			public const string DIFFUSE_SPRITE = "DiffuseSprite";
			public const string DUPLICATE_DIFFUSE_SPRITE = "DiffuseSprite";
			public const string DISCO_PARTICLE = "DiscoParticle";
			public const string DISCO_PARTICLE_DIFFUSE = "DiscoParticleDiffuse";
		}

		public static class Shaders
		{
		}

		public static class Textures
		{
			public const string TEXTURE_16PX = "16px";
			public const string DASH_1 = "dash1";
			public const string DASH_2 = "dash2";
			public const string DASH_3 = "dash3";
			public const string DASH_4 = "dash4";
			public const string DASH_5 = "dash5";
			public const string DASH_6 = "dash6";
			public const string LAMP = "lamp";
			public const string PARTICLE = "particle";
			public const string ROUND_32PX = "round32px";
			public const string STAIRS = "stairs";
			public const string ZONE = "zone";
			public const string DUPLICATE_TEXTURE_16PX = "16px";
			public const string DUPLICATE_DASH_1 = "dash1";
			public const string DUPLICATE_DASH_2 = "dash2";
			public const string DUPLICATE_DASH_3 = "dash3";
			public const string DUPLICATE_DASH_4 = "dash4";
			public const string DUPLICATE_DASH_5 = "dash5";
			public const string DUPLICATE_DASH_6 = "dash6";
			public const string DUPLICATE_LAMP = "lamp";
			public const string DUPLICATE_PARTICLE = "particle";
			public const string DUPLICATE_ROUND_32PX = "round32px";
			public const string DUPLICATE_STAIRS = "stairs";
			public const string DUPLICATE_ZONE = "zone";
		}

		public static class Custom
		{
			public const string NEW_CUSTOM_STRING = "New Custom String";
		}

	}
}
