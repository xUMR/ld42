//|||||||||||||||||||||||||||||||||||||||||||||||||||//
//|||||||||||||||||||STRING LOOKUP|||||||||||||||||||//
//|||||||||||||||||||||||||||||||||||||||||||||||||||//
//
//	String Lookup makes it easy for you to use strings without being able to track them.
//	Instead of typing "Mytag" for your tag, you would type StringLookup.Tags.MY_TAG
//
//	String Lookup auto generates your lookup library for Tags, SortingLayers, Layers
//	Materials, Shaders, Textures, Audio Files, Prefabs, Custom Strings, Scenes, Input Axes
//	And more to come in the future
//
//	VIDEO:		https://www.youtube.com/watch?v=aBON4Mqoo2A
//
//
//	HOW TO USE
//
//	1) 	Menu Item Tools > [Update] - String Lookup Library
//			-	This will update the StringLookup class
//
//	2)	Use StringLookup class to use sub class types
//			-	EX: StringLookup.Tags.PLAYER
//					This will return the "Player" tag
//
//
//	CUSTOM STRINGS //
//
//	1)	Menu Item Edit > Project Settings > Custom Strings
//
//	2)	Add Custom Strings, much like you do Tags, Layers, etc
//
//	3)	Update String Lookup Library
//
//
//	CONTACT //
//
//	TWITTER: 	https://twitter.com/ZeroLogics
//	EMAIL:		mike.desjardins@outlook.com
//	SITE:		www.mikedesjardins.ca
