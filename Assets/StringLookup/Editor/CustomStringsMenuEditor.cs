﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CustomStringsMenuEditor
{
	private static CustomStringsSO m_CustomStringsSO;

	[MenuItem("Edit/Project Settings/Custom Strings", false, 290)]
	private static void SelectCustomStrings()
	{
		m_CustomStringsSO = Resources.Load<CustomStringsSO>(StringLookupSettings.CUSTOM_STRING_SO); 

		if(m_CustomStringsSO == null)
		{
			m_CustomStringsSO = ScriptableObject.CreateInstance<CustomStringsSO>();
			m_CustomStringsSO.CustomStrings.Add("New Custom String");

			string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (StringLookupSettings.RESOURCES_FILE_PATH + StringLookupSettings.CUSTOM_STRING_ASSET);
			AssetDatabase.CreateAsset (m_CustomStringsSO, assetPathAndName);
			AssetDatabase.SaveAssets ();
			AssetDatabase.Refresh();
		}
			
		Selection.activeObject = m_CustomStringsSO;
	}
}