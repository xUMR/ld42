﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(CustomStringsSO))]
public class CustomStringsEditor : UnityEditor.Editor {

	private CustomStringsSO m_CustomStrings;
	private ReorderableList m_CustomStringList;

	private void OnEnable()
	{
		m_CustomStrings = Resources.Load<CustomStringsSO>("CustomStrings");

		if(m_CustomStrings != null)
		{
			m_CustomStringList = new ReorderableList(m_CustomStrings.CustomStrings, typeof(string), true, true, true, true);
			m_CustomStringList.elementHeight = 18;
			m_CustomStringList.drawHeaderCallback += DrawHeader;
			m_CustomStringList.drawElementCallback += DrawElement;
			m_CustomStringList.onAddCallback += OnAddCallback;
		}
	}

	private void OnDisable()
	{
		if(m_CustomStrings != null)
		{
			m_CustomStringList.drawHeaderCallback -= DrawHeader;
			m_CustomStringList.drawElementCallback -= DrawElement;
		}
	}

	public override void OnInspectorGUI()
	{
		if(m_CustomStrings == null)
		{
			OnEnable();
		}

		m_CustomStringList.DoLayoutList();
	}

	public void DrawHeader(Rect rect)
	{
		EditorGUI.LabelField(rect, "Custom Strings");
	}

	public void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
	{
		rect.height = 16;
		EditorGUI.LabelField(rect, "Custom " + index.ToString());
		rect.x += (rect.width * 0.25f);
		rect.width = (rect.width * 0.75f);
		m_CustomStrings.CustomStrings[index] = EditorGUI.TextField(rect, m_CustomStrings.CustomStrings[index]);

	}

	public void OnAddCallback(ReorderableList list)
	{
		m_CustomStrings.CustomStrings.Add("New Custom String");
	}
}
