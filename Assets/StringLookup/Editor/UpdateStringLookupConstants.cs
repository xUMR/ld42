﻿using UnityEngine;
using UnityEditorInternal;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Reflection;
using System.Linq;

public class UpdateStringLookupConstants
{
	private static string m_DisplayInfo = "";
	private static float m_Progress = 0.0f;

	private static StreamWriter m_StreamWriter;
	private static List<Action> m_ActionSequence;
	private static List<string> m_Constants;

	[MenuItem("Tools/[Update] - String Lookup Library")]
	private static void UpdateAllConstants()
	{
		m_Constants = new List<string>();
		m_ActionSequence = new List<Action>();

		LookUpUtil.CheckDirectory(StringLookupSettings.SCRIPT_FILE_PATH + StringLookupSettings.LOOKUP_FOLDER);

		string filePath = StringLookupSettings.SCRIPT_FILE_PATH + StringLookupSettings.LOOKUP_FOLDER + StringLookupSettings.PRIMARY_CLASS + ".cs";

		using(m_StreamWriter = new StreamWriter(filePath))
		{
			m_Progress = 0.0f;
			m_DisplayInfo = "Begin generating constants classes...";

			EditorUtility.DisplayProgressBar("Generating Constants", m_DisplayInfo, m_Progress);

			m_ActionSequence.Add(GenerateLookupClass);
			m_ActionSequence.Add(UpdateTagConstants);
			m_ActionSequence.Add(UpdateLayerConstants);
			m_ActionSequence.Add(UpdateSortingLayerConstants);
			m_ActionSequence.Add(UpdateAxesConstants);
			m_ActionSequence.Add(UpdateAudioConstants);
			m_ActionSequence.Add(UpdatePrefabConstants);
			m_ActionSequence.Add(UpdateSceneConstants);
			m_ActionSequence.Add(UpdateMaterialConstants);
			m_ActionSequence.Add(UpdateShaderConstants);
			m_ActionSequence.Add(UpdateTextureConstants);
	//		m_ActionSequence.Add(UpdateSpriteConstants);
			m_ActionSequence.Add(UpdateCustomConstants);

			int sequenceCount = m_ActionSequence.Count;
			int sequence = 0;
			while (sequence < sequenceCount)
			{
				m_ActionSequence[sequence]();
				m_Progress = (float)sequence++ / (float)sequenceCount;
				int percentage = (int)(m_Progress * 100f);
				string displayInfo = percentage.ToString() + "%... - " + m_DisplayInfo;
				EditorUtility.DisplayProgressBar("Generating Constants", displayInfo, m_Progress);
			}
					
			m_StreamWriter.WriteLine("\t}");
			m_StreamWriter.WriteLine("}");

			EditorUtility.ClearProgressBar();

			Clear();
		}

		AssetDatabase.Refresh();
	}

	private static void GenerateLookupClass()
	{
		bool isUniqueNamespace = (StringLookupSettings.NAMESPACE != "UnityEngine");
		if(isUniqueNamespace)
		{
			m_StreamWriter.WriteLine("using UnityEngine;");
			m_StreamWriter.WriteLine("");
		}

		m_StreamWriter.WriteLine("namespace " + StringLookupSettings.NAMESPACE);
		m_StreamWriter.WriteLine("{");
		m_StreamWriter.WriteLine("\tpublic static class " + StringLookupSettings.PRIMARY_CLASS);
		m_StreamWriter.WriteLine("\t{");
		m_StreamWriter.WriteLine("\t\t");
		m_StreamWriter.WriteLine("\t\t/* DO NOT MODIFY - UPDATING OVERWRITES */");
		m_StreamWriter.WriteLine("\t\t");
	}

	private static void GenerateClass(string _className, string _prefix, Type _type, string[] _fileExtensions, string[] _variableList = null)
	{
		if(_variableList == null)
		{
			UnityEngine.Object[] objects = LookUpUtil.GetAssetsOfType(_type, _fileExtensions);

			List<string> variableList = new List<string>();

			for(int i = 0; i < objects.Length; i++)
			{
				variableList.Add(objects[i].name);
			}

			_variableList = variableList.ToArray();
		}

		Debug.Log("[GENERATING CONSTANTS] - " + _className);
		m_DisplayInfo = "Generating Constants - " + _className;

		m_StreamWriter.WriteLine("\t\tpublic static class " + _className);
		m_StreamWriter.WriteLine("\t\t{");

		List<string> variableDuplicatesList = new List<string>();

		if(_variableList.Length > 0)
		{
			Regex regex = new Regex(@"
            (?<=[A-Z])(?=[A-Z][a-z]) |
             (?<=[^A-Z])(?=[A-Z]) |
             (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

			for (int i = 0; i < _variableList.Length; i++) 
			{
				string variableName = _variableList[i];

				variableName = variableName.Replace("\'", string.Empty)
					.Replace("\"", string.Empty)
					.Replace("/", string.Empty)
					.Replace("(", string.Empty)
					.Replace(")", string.Empty)
					.Replace(" ", string.Empty)
					.Replace(".", string.Empty)
					.Replace("_", string.Empty)
					.Replace("-", string.Empty);

				variableName = regex.Replace(variableName, "_").ToUpper();

				if(System.Char.IsDigit(variableName.ToCharArray()[0]))
				{
					variableName = _prefix + "_" + variableName;
				}

				if(variableDuplicatesList.Contains(variableName))
				{
					//	TODO:	Come up with a better way of working with duplicates, for now DUPLICATE_ tag will work
					Debug.LogWarning("Duplicate Variable Name Detected! [Variable: " + variableName + "]\n"
						+ "Adding 'DUPLICATE' prefix to variable.\nYou might want to consider giving files different names ;)");
					m_StreamWriter.WriteLine("\t\t\tpublic const string DUPLICATE_" + variableName + " = \"" + _variableList[i] + "\";");
				}
				else
				{
					variableDuplicatesList.Add(variableName);
					m_StreamWriter.WriteLine("\t\t\tpublic const string " + variableName + " = \"" + _variableList[i] + "\";");
				}
			}
		}

		m_StreamWriter.WriteLine("\t\t}");
		m_StreamWriter.WriteLine("");

		Debug.Log("[" + _prefix + "CONSTANTS GENERATED] - " + _className);
	}

	private static void UpdateTagConstants()
	{
		GenerateClass("Tags", "TAG", null, null, InternalEditorUtility.tags);
		m_Constants.Add("public static TagConstants Tags;");
	}

	private static void UpdateLayerConstants()
	{
		GenerateClass("Layers", "LAYER", null, null, InternalEditorUtility.layers);
		m_Constants.Add("public static LayerConstants Layers;");
	}

	private static void UpdateSortingLayerConstants()
	{
		Type internalEditorUtilityType = typeof(InternalEditorUtility);
		PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
		string[] sortingLayers = (string[])sortingLayersProperty.GetValue(null, new object[0]);

		GenerateClass("SortingLayers", "SORTINGLAYER", null, null, sortingLayers);
	}

	private static void UpdateAxesConstants()
	{
		UnityEngine.Object inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];
		SerializedObject obj = new SerializedObject(inputManager);
		SerializedProperty axisArray = obj.FindProperty("m_Axes");

		List<string> variableList = new List<string>();

		for(int i = 0; i < axisArray.arraySize; i++)
		{
			variableList.Add(axisArray.GetArrayElementAtIndex(i).FindPropertyRelative("m_Name").stringValue);
		}

		GenerateClass("InputAxes", "AXIS", null, null, variableList.ToArray());
	}

	private static void UpdateAudioConstants()
	{
		string[] fileExtensions = new string[]{ "wav", "ogg", "mp3" };
		GenerateClass("Audio", "AUDIO", typeof(AudioClip), fileExtensions);
	}

	private static void UpdatePrefabConstants()
	{
		string[] fileExtensions = new string[]{ "prefab" };
		GenerateClass("Prefabs", "PREFAB", typeof(GameObject), fileExtensions);
	}

	private static void UpdateSceneConstants()
	{
		string[] fileExtensions = new string[]{ "unity" };
		#if UNITY_5_3
		GenerateClass("Scenes", "SCENE", typeof(SceneAsset), fileExtensions);
		#elif UNITY_5_4
		GenerateClass("Scenes", "SCENE", typeof(SceneAsset), fileExtensions);
		#else
		GenerateClass("Scenes", "SCENE", typeof(DefaultAsset), fileExtensions);
		#endif
	}

	private static void UpdateMaterialConstants()
	{
		string[] fileExtensions = new string[]{ "mat" };
		GenerateClass("Materials", "MAT", typeof(Material), fileExtensions);
	}

	private static void UpdateShaderConstants()
	{
		string[] fileExtensions = new string[]{ "shader" };
		GenerateClass("Shaders", "SHADER", typeof(Shader), fileExtensions);
	}

	private static void UpdateTextureConstants()
	{
		string[] fileExtensions = new string[]{ "png", "jpg", "jpeg", "psd", "gif", "bmp", "tif", "tiff" };
		GenerateClass("Textures", "TEXTURE", typeof(Texture2D), fileExtensions);
	}

//	COMING SOON: Will create a look up for Sprite Sheets
//	private static void UpdateSpriteConstants()
//	{
//		UnityEngine.Object[] sprites = GetAssetsOfType(typeof(Sprite), "png", "jpg", "jpeg", "psd", "gif", "bmp", "tif", "tiff");
//
//		List<string> variableList = new List<string>();
//
//		for(int i = 0; i < sprites.Length; i++)
//		{
//			variableList.Add(sprites[i].name);
//		}
//
//		Object[] objects = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(filename));
//		Sprite[] sprites = objects.Select(x=>(x as Sprite)).ToArray();
//
//		GenerateClass("SpriteConstants", variableList.ToArray(), "SPRITE");
//	}

	private static void UpdateCustomConstants()
	{
		CustomStringsSO customStrings = Resources.Load<CustomStringsSO>(StringLookupSettings.CUSTOM_STRING_SO);

		if(customStrings != null)
		{
			GenerateClass("Custom", "CUSTOM", null, null, customStrings.CustomStrings.ToArray());
		}
	}

	private static void Clear()
	{
		if(m_ActionSequence != null)
		{
			m_ActionSequence.Clear();
			m_ActionSequence = null;
		}

		if(m_Constants != null)
		{
			m_Constants.Clear();
			m_Constants = null;
		}
			
		m_StreamWriter.Close();
		m_StreamWriter = null;
	}
}

public static class LookUpUtil
{
	public static void CheckDirectory(string _directory)
	{
		if(!System.IO.Directory.Exists(_directory))
		{
			System.IO.Directory.CreateDirectory(_directory);
			Debug.Log("[CREATING DIRECTORY] - Constants Directory didn't exist, creating directory: " + _directory);
		}
	}

	public static UnityEngine.Object[] GetAssetsOfType(System.Type type, params string[] fileExtensions)
	{
		List<UnityEngine.Object> tempObjects = new List<UnityEngine.Object>();
		DirectoryInfo directory = new DirectoryInfo(Application.dataPath);

		for(int i = 0; i < fileExtensions.Length; i++)
		{
			fileExtensions[i] = "*." + fileExtensions[i];
		}

		FileInfo[] goFileInfo = fileExtensions
			.SelectMany(i => directory.GetFiles(i, SearchOption.AllDirectories))
			.ToArray();

		for (int i = 0; i < goFileInfo.Length; i++)
		{
			FileInfo tempGoFileInfo = goFileInfo[i];

			if (tempGoFileInfo == null)
			{
				continue;
			}

			string tempFilePath = tempGoFileInfo.FullName;
			tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");
			UnityEngine.Object tempGO = AssetDatabase.LoadAssetAtPath(tempFilePath, typeof(UnityEngine.Object)) as UnityEngine.Object;

			if (tempGO == null)
			{
				continue;
			}
			else if (tempGO.GetType() != type)
			{
				continue;
			}

			tempObjects.Add(tempGO);
		}

		return tempObjects.ToArray();
	}
}